def BinarySearch(a,low,high,data) 
    mid = (high + low)/2
   
    if(data == a[mid])
        return a[mid]
    
    elseif data < a[mid]
        BinarySearch(a,low,mid-1,data)
    else
        BinarySearch(a,mid+1,high,data)
    end
end


a =[1, 12, 23, 33, 56, 64, 75, 90, 100]

found = BinarySearch(a,0,a.size-1,75)
puts "Item #{found} found"
