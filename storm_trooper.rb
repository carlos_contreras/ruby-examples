class StormTrooper < ImperialSoldier
    def initialize
        puts "Storm trooper is initialized"
    end
    
    def shoot
        puts "Storm trooper shoot, but he misses... as always"
    end
   
    def salute
        puts "Storm trooper salutes, his name is Gary... Gary the storm trooper"
    end

    def method_missing(method_name, *args)
        puts "Gary the storm trooper can't handle this method:#{method_name}"
    end
end