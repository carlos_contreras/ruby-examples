def quickSort(a,low,high)
    if low < high
        pivot = partition(a,low,high)
        quickSort(a,low,pivot-1)
        quickSort(a,pivot+1,high)
    end
end

def partition(a,low,high)
    pivot_item = a[low]
    left = low
    right = high
    while left < right do
        while a[left] <= pivot_item do
            left = left +1
        end
        while a[right] > pivot_item do
            right = right -1
        end
        if left < right
            a[left],a[right] = a[right],a[left]
        end
    end
    a[low] = a[right]
    a[right] = pivot_item
    right
end

a = [1,56,33,23,75,64,90,100,64,56,12]
puts "my unordered list : #{a}"
left = 0
right = 10
quickSort(a,left,right)
puts "my ordered list : #{a}"