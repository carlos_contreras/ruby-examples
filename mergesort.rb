def mergeSort(a, temp, left, right) #O(nlogn)
    if left < right
        mid = (right+left)/2
        mergeSort(a,temp,left,mid)
        mergeSort(a,temp,mid+1,right)
        merge(a,temp,left,mid+1,right)
    end
end
def merge(a, temp, left, mid, right)
    left_end = mid-1
    size = (right-left) + 
    temp_pos = left

    while left <= left_end && mid <= right do
        if a[left] <= a[mid]
            temp[temp_pos]= a[left]
            left = left + 1
            temp_pos = temp_pos + 1
        else
            temp[temp_pos]= a[mid]
            mid = mid + 1 
            temp_pos = temp_pos +1
        end
    end

    while left <= left_end
        temp[temp_pos] = a[left]
        left = left + 1
        temp_pos = temp_pos + 1
    end
    while mid <= right
        temp[temp_pos] = a[mid]
        mid = mid + 1
        temp_pos = temp_pos + 1
    end

    for i in 0..size
        a[right] = temp[right]
        right = right -1
    end
end

a = [1,56,33,23,75,64,90,100,64,56,12]
t = []
puts "my unordered list : #{a}"
left = 0
right = 10
mergeSort(a,t,left,right)
puts "my ordered list : #{a}"

