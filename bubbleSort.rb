def BubbleSort(a) #O(n^2)
    for i in 0..a.size-1
        for j in 0..a.size-1
            if a[i] < a[j]
                a[j],a[i] = a[i],a[j]
            end
        end
    end
end

a = [1,56,33,23,75,64,90,100,64,56,12]
puts "my unordered list : #{a}"
left = 0
right = 10
BubbleSort(a)
puts "my ordered list : #{a}"