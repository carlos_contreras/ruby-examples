$LOAD_PATH << File.dirname('imperial_soldier')
$LOAD_PATH << File.dirname('storm_trooper')

require 'imperial_soldier'
require 'storm_trooper'

soldier = StormTrooper.new
soldier.salute
soldier.march
soldier.shoot
soldier.die

soldier.instance_eval do
    self.class.class_eval do 
        define_method :die do
            puts "#{self.class} died... :("
        end
    end
end

soldier.die

