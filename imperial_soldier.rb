class ImperialSoldier
    def initialize
        puts "The imperial soldier is initialized"
    end

    def shoot
        puts "The imperial soldier shoot"
    end

    def march
        puts "The imperial soldier marches"
    end

    def salute
        puts "The imperial soldier salutes"
    end

    def method_missing(method_name, *args)
        puts "The imperial soldier can't handle this method:#{method_name}"
    end
end